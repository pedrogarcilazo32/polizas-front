import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'polizas',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
