import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  {
    path: 'polizas',
    loadChildren: () =>
      import('./views/polizas/polizas.module').then((m) => m.PolizasPageModule),
  },
  {
    path: '',
    redirectTo: 'principal',
    pathMatch: 'full',
  },

  {
    path: 'principal',
    loadChildren: () =>
      import('./views/principal/principal.module').then(
        (m) => m.PrincipalPageModule
      )
  },















];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
