/* eslint-disable no-trailing-spaces */
/* eslint-disable curly */
/* eslint-disable no-var */
/* eslint-disable one-var */
import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
})
export class FormatService {

    formatDate(date:any, caracter:any) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join(caracter);
    }

    formatDateMXN(date:any, caracter:any) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [day, month, year].join(caracter);
    }

    getHora(date:any) {
        var d = new Date(date),
            hour = '' + d.getHours();

        if (hour.length < 2)
            hour = '0' + hour;

        return hour;
    }

    getMinutos(date:any) {
        var d = new Date(date),
            minutes = '' + d.getMinutes();

        if (minutes.length < 2)
            minutes = '0' + minutes;

        return minutes;
    }
    getSegundos(date:any){
        var d = new Date(date),
        seconds = '' + d.getSeconds();
        if (seconds.length < 2)
        seconds = '0' + seconds;
        return seconds
    }

    getHora24(date:any){
        var time = new Date(date);
        var hour=''
        hour= time.toLocaleString('en-US', { hour: 'numeric', minute: '2-digit', second: '2-digit', hour12: true });

        return hour
    }
    getHora24Form(date:any){
      var time = new Date(date);
      var hour=''
      hour= time.toLocaleString('en-US', { hour: 'numeric', minute: '2-digit', second: '2-digit', hour12: false });

      return hour
  }
  getHora24FormAct(){
    var time = new Date();
    var hour=''
    hour= time.toLocaleString('en-US', { hour: 'numeric', minute: '2-digit', second: '2-digit', hour12: false });

    return hour
}

    getDia(date:any) {
        var d = new Date(date),
            day = '' + d.getDate();

        if (day.length < 2)
            day = '0' + day;

        return day;
    }

    getMes(date:any) {
        var d = new Date(date),
            month = '' + (d.getMonth()+1);

        if (month.length < 2)
            month = '0' + month;

        return month;
    }

    getAnio(date:any) {
        var d = new Date(date),
            year = d.getFullYear();

        return year;
    }

    getNombreDia(date:any) {
        var d = new Date(date),
            month = (d.getDay() + 1);
        var dia = ''
        if (month == 1) dia= 'Domingo';
        if (month == 2) dia=  'Lunes';
        if (month == 3) dia=  'Martes';
        if (month == 4) dia=  'Miércoles';
        if (month == 5) dia=  'Jueves';
        if (month == 6) dia=  'Viernes';
        if (month == 7) dia=  'Sábado';
        return dia;
    }

    getNombreMes(date:any) {
        var d = new Date(date),
            month = (d.getMonth() + 1);
        var mes =''
        if (month == 1) mes= 'Enero';
        if (month == 2) mes= 'Febrero';
        if (month == 3) mes= 'Marzo';
        if (month == 4) mes= 'Abril';
        if (month == 5) mes= 'Mayo';
        if (month == 6) mes= 'Junio';
        if (month == 7) mes= 'Julio';
        if (month == 8) mes= 'Agosto';
        if (month == 9) mes= 'Septiembre';
        if (month == 10) mes= 'Octubre';
        if (month == 11) mes= 'Noviembre';
        if (month == 12) mes= 'Diciembre';
        return mes
    }

    getNombreMesCorto(date:any) {
        var d = new Date(date),
            month = (d.getMonth() + 1);
        var mes='';
        if (month == 1) mes= 'Ene';
        if (month == 2) mes= 'Feb';
        if (month == 3) mes= 'Mar';
        if (month == 4) mes= 'Abr';
        if (month == 5) mes= 'May';
        if (month == 6) mes= 'Jun';
        if (month == 7) mes= 'Jul';
        if (month == 8) mes= 'Ago';
        if (month == 9) mes= 'Sep';
        if (month == 10) mes= 'Oct';
        if (month == 11) mes= 'Nov';
        if (month == 12) mes= 'Dic';
        return mes
    }

    getPrimerDiaMes(date:any) {
        return new Date(date.getFullYear(), date.getMonth(), 1);
    }

    getUltimoDiaMes(date:any) {
        return new Date(date.getFullYear(), date.getMonth() + 1, 0);
    }

    acomodaDecimales(event:any){;
        return event.toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits:2})
    }

    acomodaDecimalesMillones(event:any){
        let num = event / 1000000;
        let numF = num.toLocaleString('en-US', {minimumFractionDigits: 2, maximumFractionDigits:2});
        return numF + " M"
    }

    public limpiarAcentos(texto: string): string {
        let limpio = texto;

        if(!texto)
          return "";

        let acento = [
          {a: "á", b: "a"},
          {a: "é", b: "e"},
          {a: "í", b: "i"},
          {a: "ó", b: "o"},
          {a: "ú", b: "u"},
          {a: "Á", b: "A"},
          {a: "É", b: "E"},
          {a: "Í", b: "I"},
          {a: "Ó", b: "O"},
          {a: "Ú", b: "U"},
        ];

        acento.forEach(x =>{
          limpio = limpio.split(x.a).join(x.b);
        });

        return limpio;
    }
    public fechaDefaultInput(caracter,formato){
      var fecha=new Date().toLocaleDateString('es-MX').split('T')[0]
    var divisiones = fecha.split("/");
    var day = divisiones[0]
    var month = divisiones [1]
    var year = divisiones[2]
      var cadena=""
      if(formato==1){
        cadena=[year, month.length==1 ? '0'+month:month, day.length==1? '0'+day:day].join(caracter)
      }
      else if (formato==2){
cadena=[ day.length==1? '0'+day:day, month.length==1 ? '0'+month:month,year].join(caracter)
      }
    return cadena
    }
    public fechaDefaultInputHora(caracter,formato){
      var fecha=new Date().toLocaleDateString().split('T')[0]
    var divisiones = fecha.split("/");
    var day = divisiones[0]
    var month = divisiones [1]
    var year = divisiones[2]
      var cadena=""
      if(formato==1){
        cadena=[year, month.length==1 ? '0'+month:month, day.length==1? '0'+day:day].join(caracter)+' '+this.getHora24(new Date().toLocaleString())
      }
      else if (formato==2){
cadena=[ day.length==1? '0'+day:day, month.length==1 ? '0'+month:month,year].join(caracter)+' '+this.getHora24(new Date().toLocaleString())
      }
      else if (formato==3){
        cadena=[ day.length==1? '0'+day:day, month.length==1 ? '0'+month:month,year].join(caracter)+' '+this.getHora24FormAct()
              }
    return cadena
    }
    NumeroALetras(num, currency) {
      currency = currency || {};
      let data = {
        numero: num,
        enteros: Math.floor(num),
        centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
        letrasCentavos: '',
        letrasMonedaPlural: currency.plural ||'PESOS',// 'Dólares', 'Bolívares', 'etcs'
        letrasMonedaSingular: currency.singular || 'PESO',// 'Dólar', 'Bolivar', 'etc'
        letrasMonedaCentavoPlural: currency.centPlural || 'CENTAVOS',
        letrasMonedaCentavoSingular: currency.centSingular || 'CENTAVO'
      };



      if (data.enteros == 0)
        return 'CERO ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
      if (data.enteros == 1)
        return this.Millones(data.enteros) + ' ' + data.letrasMonedaSingular + ' ' + data.letrasCentavos;
      else
        return this.Millones(data.enteros) + ' ' + data.letrasMonedaPlural + ' ' + data.letrasCentavos;
    };
    Miles(num) {
      let divisor = 1000;
      let cientos = Math.floor(num / divisor)
      let resto = num - (cientos * divisor)

      let strMiles = this.Seccion(num, divisor, 'UN MIL', 'MIL');
      let strCentenas = this.Centenas(resto);

      if (strMiles == '')
        return strCentenas;

      return strMiles + ' ' + strCentenas;
    } //Miles()

    Millones(num) {
      let divisor = 1000000;
      let cientos = Math.floor(num / divisor)
      let resto = num - (cientos * divisor)

      let strMillones = this.Seccion(num, divisor, 'UN MILLON DE', 'MILLONES DE');
      let strMiles = this.Miles(resto);
      if (strMillones == '')
        return strMiles;

      return strMillones + ' ' + strMiles;
    } //Millones()
    Unidades(num) {

      switch (num) {
        case 1:
          return 'UN';
        case 2:
          return 'DOS';
        case 3:
          return 'TRES';
        case 4:
          return 'CUATRO';
        case 5:
          return 'CINCO';
        case 6:
          return 'SEIS';
        case 7:
          return 'SIETE';
        case 8:
          return 'OCHO';
        case 9:
          return 'NUEVE';
      }

      return '';
    } //Unidades()

    Decenas(num) {

      let decena = Math.floor(num / 10);
      let unidad = num - (decena * 10);

      switch (decena) {
        case 1:
          switch (unidad) {
            case 0:
              return 'DIEZ';
            case 1:
              return 'ONCE';
            case 2:
              return 'DOCE';
            case 3:
              return 'TRECE';
            case 4:
              return 'CATORCE';
            case 5:
              return 'QUINCE';
            default:
              return 'DIECI' + this.Unidades(unidad);
          }
        case 2:
          switch (unidad) {
            case 0:
              return 'VEINTE';
            default:
              return 'VEINTI' + this.Unidades(unidad);
          }
        case 3:
          return this.DecenasY('TREINTA', unidad);
        case 4:
          return this.DecenasY('CUARENTA', unidad);
        case 5:
          return this.DecenasY('CINCUENTA', unidad);
        case 6:
          return this.DecenasY('SESENTA', unidad);
        case 7:
          return this.DecenasY('SETENTA', unidad);
        case 8:
          return this.DecenasY('OCHENTA', unidad);
        case 9:
          return this.DecenasY('NOVENTA', unidad);
        case 0:
          return this.Unidades(unidad);
      }
    } //Unidades()

    DecenasY(strSin, numUnidades) {
      if (numUnidades > 0)
        return strSin + ' Y ' + this.Unidades(numUnidades)

      return strSin;
    } //DecenasY()

    Centenas(num) {
      let centenas = Math.floor(num / 100);
      let decenas = num - (centenas * 100);

      switch (centenas) {
        case 1:
          if (decenas > 0)
            return 'CIENTO ' + this.Decenas(decenas);
          return 'CIEN';
        case 2:
          return 'DOSCIENTOS ' + this.Decenas(decenas);
        case 3:
          return 'TRESCIENTOS ' + this.Decenas(decenas);
        case 4:
          return 'CUATROCIENTOS ' + this.Decenas(decenas);
        case 5:
          return 'QUINIENTOS ' + this.Decenas(decenas);
        case 6:
          return 'SEISCIENTOS ' + this.Decenas(decenas);
        case 7:
          return 'SETECIENTOS ' + this.Decenas(decenas);
        case 8:
          return 'OCHOCIENTOS ' + this.Decenas(decenas);
        case 9:
          return 'NOVECIENTOS ' + this.Decenas(decenas);
      }

      return this.Decenas(decenas);
    } //Centenas()

    Seccion(num, divisor, strSingular, strPlural) {
      let cientos = Math.floor(num / divisor)
      let resto = num - (cientos * divisor)

      let letras = '';

      if (cientos > 0)
        if (cientos > 1)
          letras = this.Centenas(cientos) + ' ' + strPlural;
        else
          letras = strSingular;

      if (resto > 0)
        letras += '';

      return letras;
    }
    public fechaIncioFinDefault(cad:number){
      var fec;
      var date= new Date();
      var fecha=new Date().toLocaleDateString().split('T')[0]
      var primerDia = new Date(date.getFullYear(), date.getMonth(), 1);
    var ultimoDia = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    var divisiones = fecha.split("/");
    var day = cad==1?""+primerDia.getDate():""+ultimoDia.getDate()
    var month = divisiones [1]
    var year = divisiones[2]


    console.log(primerDia.getDate(),ultimoDia.getDate())

      if(cad==1)
    {
      fec= [year, month.length==1 ? '0'+month:month, day.length==1? '0'+day:day].join('-')
    }
    else
    {
    fec=[year, month.length==1 ? '0'+month:month, day.length==1? '0'+day:day].join('-')
    }
    return fec
    }
}
