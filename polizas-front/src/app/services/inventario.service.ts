import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class InventarioService {
  url: string = environment.url;
  constructor() {}
  public obtenerInventariosActivos() {
    return new Promise((resolve, reject) => {
      fetch(this.url + 'inventario/obtenerInventariosActivos', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          if (response.status === 200) {
            console.log(response)
            response.json().then((data: any) => {
              if (Object.entries(data).length === 0) {
                resolve(null);
              } else {
                resolve(data);
              }
            });
          } else {
            reject('No se pudieron obtener los inventarios');
          }
        })
        .catch((e) => reject(e));
    });
  }
}
