import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Empleado } from '../models/empleado.model';
@Injectable({
  providedIn: 'root',
})
export class EmpleadoService {
  url: string = environment.url;
  constructor() {}
  public obtenerEmpleadosActivos() {
    return new Promise((resolve, reject) => {
      fetch(this.url + 'empleado/obtenerEmpleadosActivos', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          if (response.status === 200) {
            console.log(response)
            response.json().then((data: any) => {
              if (Object.entries(data).length === 0) {
                resolve(null);
              } else {
                resolve(data);
              }
            });
          } else {
            reject('No se pudieron obtener los empleados');
          }
        })
        .catch((e) => reject(e));
    });
  }
  public obtenerEmpleadoPorId(id:number) {
    return new Promise((resolve, reject) => {
      fetch(this.url + `empleado/obtenerEmpleadoPorId/${id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          if (response.status === 200) {
            console.log(response)
            response.json().then((data: any) => {
              if (Object.entries(data).length === 0) {
                resolve(null);
              } else {
                resolve(data);
              }
            });
          } else {
            reject('No se pudo obtener el empleado');
          }
        })
        .catch((e) => reject(e));
    });
  }
  public actualizarEmpleado(data:Empleado) {
    return new Promise((resolve, reject) => {
      fetch(this.url + 'empleado/actualizarEmpleado', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',

        },
      })
        .then((response) => {
          if (response.status === 200) {
            console.log(response)
            response.json().then((data: any) => {
              if (Object.entries(data).length === 0) {
                resolve(null);
              } else {
                resolve(data);
              }
            });
          } else {
            reject('No se pudo actualizar el empleado');
          }
        })
        .catch((e) => reject(e));
    });
  }
}
