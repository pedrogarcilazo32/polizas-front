import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Poliza } from '../models/poliza.model';
@Injectable({
  providedIn: 'root',
})
export class PolizasService {
  url: string = environment.url;
  constructor() {}
  public generarPoliza(data:Poliza) {
    return new Promise((resolve, reject) => {
      fetch(this.url + 'poliza/generarPoliza', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          if (response.status === 200) {
            response.json().then((data: any) => {
              if (Object.entries(data).length === 0) {
                resolve(null);
              } else {
                resolve(data);
              }
            });
          } else {
            reject('No se pudo generar la poliza');
          }
        })
        .catch((e) => reject(e));
    });
  }
  public obtenerPolizas() {
    return new Promise((resolve, reject) => {
      fetch(this.url + 'poliza/obtenerPolizas', {
        method: 'GET',

        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          if (response.status === 200) {
            response.json().then((data: any) => {
              if (Object.entries(data).length === 0) {
                resolve(null);
              } else {
                resolve(data);
              }
            });
          } else {
            reject('No se pudieron obtener las polizas');
          }
        })
        .catch((e) => reject(e));
    });
  }
  public obtenerPolizasPaginado(data:Poliza) {
    return new Promise((resolve, reject) => {
      fetch(this.url + 'poliza/obtenerPolizasPaginado', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          if (response.status === 200) {
            response.json().then((data: any) => {
              if (Object.entries(data).length === 0) {
                resolve(null);
              } else {
                resolve(data);
              }
            });
          } else {
            reject('No se pudo obtener el paginado');
          }
        })
        .catch((e) => reject(e));
    });
  }
  public desactivarPoliza(id:number) {
    return new Promise((resolve, reject) => {
      fetch(this.url + `poliza/desactivarPoliza/${id}`, {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then((response) => {
          if (response.status === 200) {
            response.json().then((data: any) => {
              if (Object.entries(data).length === 0) {
                resolve(null);
              } else {
                resolve(data);
              }
            });
          } else {
            reject('No se pudo desactivar la poliza');
          }
        })
        .catch((e) => reject(e));
    });
  }
}
