/* eslint-disable no-trailing-spaces */
/* eslint-disable curly */
/* eslint-disable no-var */
/* eslint-disable one-var */
import { Injectable } from '@angular/core';
import { Validators } from '@angular/forms';
@Injectable({
  providedIn: 'root',
})

export class ValidatorService {
    soloLetras(e:any,tipo:any) {
        var key = e.keyCode || e.which,
          tecla = String.fromCharCode(key).toLowerCase(),
          especiales = [8, 37, 39, 46],
          tecla_especial = false;
        if(tipo=="numerosLetras"){
          var  letras = " 1234567890abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
        }
        else{
          var  letras = " áéíóúabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ"
        }
        for (var i in especiales) {
          if (key == especiales[i]) {
            tecla_especial = true;
            break;
          }
        }
        if (letras.indexOf(tecla) == -1 ) {
          return false;
        }
        else{
          return true
        }
      }
      soloNumeros(e:any) {
        var key = e.keyCode || e.which,
          tecla = String.fromCharCode(key).toLowerCase();
          var  letras = "1234567890"
        if (letras.indexOf(tecla) == -1 ) {
          return false;
        }
        else{
          return true
        }
      }
      soloNumerosDecimal(e:any) {
        var key = e.keyCode || e.which,
          tecla = String.fromCharCode(key).toLowerCase();
          var  letras = ".1234567890"
        if (letras.indexOf(tecla) == -1 ) {
          return false;
        }
        else{
          return true
        }
      }
}
