import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MessagingService } from 'src/app/services/messaging.service';
import { LoadingService } from 'src/app/services/loading.service';

import { AlertController } from '@ionic/angular';
import { ValidatorService } from 'src/app/services/validators.service';
import { ModalController } from '@ionic/angular';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { Empleado } from 'src/app/models/empleado.model';
import { Inventario } from 'src/app/models/inventario.model';
import { InventarioService } from 'src/app/services/inventario.service';
import { Poliza } from 'src/app/models/poliza.model';
import { PolizasService } from 'src/app/services/polizas.services';
@Component({
  selector: 'app-crear-poliza',
  templateUrl: './crear-poliza.component.html',
  styleUrls: ['./crear-poliza.component.scss'],
})
export class CrearPolizaComponent  implements  OnInit {
  polizasForm:FormGroup;
  empleadoslist:Array<Empleado>
  inventarioslist:Array<Inventario>
  cantidadProducto=0;
  validationMessages = {
    empleado: [
      { type: 'required', message: 'El empleado es requerido' },


    ],
    producto: [
      { type: 'required', message: 'El producto requerido' },


    ],
    cantidad: [
      { type: 'required', message: 'La cantidad es requerida' },


    ],


  };
  constructor(
private formBuilder: FormBuilder,
private MessagingService:MessagingService,
private loadingService:LoadingService,
private alertCon:AlertController,
private ValidatorService:ValidatorService,
private ModalController:ModalController,
private EmpleadoService:EmpleadoService,
private InventarioService:InventarioService,
private PolizasService:PolizasService

  ) {

    this.polizasForm = this.formBuilder.group({
      empleado: new FormControl("", Validators.compose([Validators.required])),
      producto: ['', [
        Validators.required,

      ]],
      cantidad: new FormControl("", Validators.compose([Validators.required])),

    });
  }

  ngOnInit() {
this.obtenerEmpleadosActivos();


  }

  cerrarSinData(){
    this.ModalController.dismiss();
  }
  soloNumeros(e:any) {
    return this.ValidatorService.soloNumeros(e);
  }

generar(credentials: any) {
  console.log(credentials)
if(this.cantidadProducto<credentials.cantidad){
  this.MessagingService.warning("La cantidad no puede ser mayor a el total de el producto")
  return
}
else{
  var obj = new Poliza();
  obj.idEmpleado=credentials.empleado;
  obj.sku_cod=credentials.producto;
  obj.cantidad=credentials.cantidad;
  this.generarPoliza(obj)
}
}
asignarCantidad(credentials){
  let sku_cod=credentials.producto
  this.cantidadProducto=this.inventarioslist.filter((element : Inventario)  => element.sku_cod == sku_cod)[0].cantidad
  console.log(this.cantidadProducto)
}










  async comfirmarAlert(credentials) {
    this.loadingService.dismiss();
    const alert = await this.alertCon.create({
      cssClass: 'alert-class',
      header: 'Información ',
      message: '¿Está seguro de continuar?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
        {
          text: 'Aceptar',
          handler: () => {

            this.generar(credentials)

          },
        },
      ],
    });

    await alert.present();
  }

  //PETICIONES API
  obtenerEmpleadosActivos(){
    this.loadingService.present();
   this.EmpleadoService.obtenerEmpleadosActivos()
     .then((response: any) => {
       this.loadingService.dismiss();
      console.log(response)
      if(response.estatus){
        if(response.empleados.length){
          this.empleadoslist=response.empleados
          this.obtenerInventariosActivos();
        }
        else{
          this.MessagingService.warning("No se encontraron empleados activos")
        }
      }
      else{
        this.MessagingService.error("No se pudo realizar la operacion")
      }
     })
     .catch((e) => {
      this.MessagingService.error("Hubo un error al realizar la operacion")

       console.log(e);
       this.loadingService.dismiss();

     });
}
obtenerInventariosActivos(){
  this.loadingService.present();
 this.InventarioService.obtenerInventariosActivos()
   .then((response: any) => {
     this.loadingService.dismiss();
    console.log(response)
    if(response.estatus){
      if(response.inventarios.length){
        this.inventarioslist=response.inventarios
        console.log(this.inventarioslist)
      }
      else{
        this.MessagingService.warning("No se encontraron inventarios activos")
      }
    }
    else{
      this.MessagingService.error("No se pudo realizar la operacion")
    }
   })
   .catch((e) => {
    this.MessagingService.error("Hubo un error al realizar la operacion")

     console.log(e);
     this.loadingService.dismiss();

   });
}
generarPoliza(data:Poliza){
  this.loadingService.present();
 this.PolizasService.generarPoliza(data)
   .then((response: any) => {
     this.loadingService.dismiss();
     console.log(response)
    if(response.estatus){
      this.MessagingService.success("Poliza generada correctamente")
      this.cerrarSinData();
    }
    else{
      this.MessagingService.warning("No se pudo generar la poliza")
    }

   })
   .catch((e) => {
    this.MessagingService.error("Hubo un error al realizar la operacion")

     console.log(e);
     this.loadingService.dismiss();

   });
}


}

