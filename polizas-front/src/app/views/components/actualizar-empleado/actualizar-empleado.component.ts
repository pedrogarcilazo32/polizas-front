import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MessagingService } from 'src/app/services/messaging.service';
import { LoadingService } from 'src/app/services/loading.service';

import { AlertController } from '@ionic/angular';
import { ValidatorService } from 'src/app/services/validators.service';
import { ModalController } from '@ionic/angular';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { Empleado } from 'src/app/models/empleado.model';
import { Inventario } from 'src/app/models/inventario.model';
import { InventarioService } from 'src/app/services/inventario.service';
import { Poliza } from 'src/app/models/poliza.model';
import { PolizasService } from 'src/app/services/polizas.services';
@Component({
  selector: 'app-actualizar-empleado',
  templateUrl: './actualizar-empleado.component.html',
  styleUrls: ['./actualizar-empleado.component.scss'],
})
export class ActualizarEmpleadoComponent implements  OnInit {
  empleadoForm:FormGroup;
  id:any;
  empleadoObj:Empleado;
  validationMessages = {
    nombre: [
      { type: 'required', message: 'El nombre es requerido' },
      {type:'minLength',message:"Se requieren por lo menos 5 caracteres  "}

    ],
    apellido: [
      { type: 'required', message: 'El producto requerido' },
      {type:'minLength',message:"Se requieren por lo menos 5 caracteres  "}

    ],


  };
  constructor(
private formBuilder: FormBuilder,
private MessagingService:MessagingService,
private loadingService:LoadingService,
private alertCon:AlertController,
private ValidatorService:ValidatorService,
private ModalController:ModalController,
private EmpleadoService:EmpleadoService,

  ) {

    this.empleadoForm = this.formBuilder.group({
      nombre: new FormControl("", Validators.compose([Validators.required,Validators.minLength(5)])),
      apellido: ['', [
        Validators.required,
        Validators.minLength(5)
      ]],
      puesto: ['', [


      ]],


    });
  }

  ngOnInit() {

this.obtenerEmpleadoPorId()
  }

  cerrarSinData(){
    this.ModalController.dismiss();
  }





  async comfirmarAlert(credentials) {
    this.loadingService.dismiss();
    const alert = await this.alertCon.create({
      cssClass: 'alert-class',
      header: 'Información ',
      message: '¿Está seguro de continuar?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
        {
          text: 'Aceptar',
          handler: () => {

            this.actualizar(credentials)

          },
        },
      ],
    });

    await alert.present();
  }

  //PETICIONES API
  actualizar(credentials: any) {
    if(credentials.nombre.trim()==0){
      this.MessagingService.warning("Por favor indique un nombre")
      return
    }
    else if(credentials.apellido.trim()==0){
      this.MessagingService.warning("Por favor indique un apellido")
      return
    }
    let emp = new Empleado();
    emp.idEmpleado=this.id;
    emp.nombre=credentials.nombre;
    emp.apellido=credentials.apellido;
    this.loadingService.present();
    this.EmpleadoService.actualizarEmpleado(emp)
      .then((response: any) => {
        this.loadingService.dismiss();
       console.log(response)
       if(response.estatus){
        this.MessagingService.success("Se actualizó correctamente al empleado");
        this.ModalController.dismiss();
       }
       else{
         this.MessagingService.error("No se pudo realizar la operacion")
       }
      })
      .catch((e) => {
       this.MessagingService.error("Ha ocurrido un error al intentar actualizar el empleado")

        console.log(e);
        this.loadingService.dismiss();

      });
  }
  obtenerEmpleadoPorId(){
    this.loadingService.present();
   this.EmpleadoService.obtenerEmpleadoPorId(this.id)
     .then((response: any) => {
       this.loadingService.dismiss();
      console.log(response)
      if(response.estatus){
        if(response.empleados.length){
          this.empleadoObj=response.empleados[0]
          this.empleadoForm = this.formBuilder.group({
            nombre: this.empleadoObj.nombre,
            apellido: this.empleadoObj.apellido,
            puesto: this.empleadoObj.puestoNombre,
          });
        }
        else{
          this.MessagingService.warning("No se encontro al empleado")
        }
      }
      else{
        this.MessagingService.error("No se pudo realizar la operacion")
      }
     })
     .catch((e) => {
      this.MessagingService.error("Hubo un error al realizar la operacion")

       console.log(e);
       this.loadingService.dismiss();

     });
}


}

