import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrincipalPage } from './principal.page';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'polizas',
    pathMatch: 'full',
  },
  {
    path: '',
    component: PrincipalPage,
    children: [
      {
        path: 'polizas',
        loadChildren: () =>
          import('../polizas/polizas.module').then((m) => m.PolizasPageModule),

      },


    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrincipalPageRoutingModule {}
