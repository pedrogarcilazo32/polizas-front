import { element } from 'protractor';
import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/services/loading.service';
import { MessagingService } from 'src/app/services/messaging.service';
import { AlertController } from '@ionic/angular';
import { PolizasService } from 'src/app/services/polizas.services';
import { ModalController } from '@ionic/angular';
import { CrearPolizaComponent } from '../components/crear-poliza/crear-poliza.component';
import { Poliza } from 'src/app/models/poliza.model';
import { FormatService } from 'src/app/services/format.service';
import { ActualizarEmpleadoComponent } from '../components/actualizar-empleado/actualizar-empleado.component';
@Component({
  selector: 'app-polizas',
  templateUrl: './polizas.page.html',
  styleUrls: ['./polizas.page.scss'],
})
export class PolizasPage implements  OnInit {
  pagina: number = 1;
  total_paginas: number = 1;
  paginaAux: number = 1;
  busqueda:string = "";
  paginado:number=20
  listadoPolizas=[];
  polizasExcel=[];
  constructor(
    private messasing: MessagingService,
    private loadingService: LoadingService,
    private PolizasService:PolizasService,
    private alertCon:AlertController,
    private ModalController:ModalController,
    private FormatService:FormatService
  ) { }

  ngOnInit() {

    this.obtenerPolizasPaginado();
    this.obtenerPolizas();
  }
  editar(valores: any) {
    // localStorage.setItem('editarCaja', JSON.stringify(valores));

    // this.agregarCaja();
  }

  obtenerFechaArchivo(fecha:any) {
    // let date = new Date(fecha);
    // return  this.FormatService.formatDateMXN(fecha,"_");
  }
  obtenerFecha(fecha:any) {
    let date = new Date(fecha);
    return this.FormatService.formatDate(date,'/') + ' ' + this.FormatService.getHora24(date);

  }
  async comfirmarAlert(data: any) {
    const alert = await this.alertCon.create({
      cssClass: 'alert-class',
      header: 'Confirmación',
      message: '¿Está seguro de continuar?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {},
        },
        {
          text: 'Aceptar',
          handler: () => {
          this.desactivarPoliza(data)
          },
        },
      ],
    });

    await alert.present();
  }
//CREAR REPORTE
crearExcel() {

  if(this.polizasExcel.length){
    var html_table = "";
    html_table +=
      '<meta charset="utf-8"><tr align="center">' +
      '<th colspan="1">ID</th>' +
      '<th colspan="1">Empleado</th>' +
      '<th colspan="1">Sku inventario</th>' +
      '<th colspan="1">Cantidad</th>' +
      '<th colspan="1">Registro fecha</th>' +
      '<th colspan="1">Estado</th>' +
      '</tr>';


      this.polizasExcel.forEach((item:Poliza) => {
        var estatus= item.activo_opc? 'Activo':'Desactivada'
        var fecha=this.obtenerFecha(item.fecha_reg)
        html_table += '<tr align="left">';
        html_table += '<td colspan="1"> ' + item.idPoliza + '</td>';
        html_table += '<td colspan="1"> ' + item.nombreEmpleado + '</td>';
        html_table += '<td colspan="1"> ' + item.sku_cod + '</td>';
        html_table += '<td colspan="1"> ' + item.cantidad + '</td>';
        html_table += '<td colspan="1"> ' + fecha + '</td>';
        html_table += '<td colspan="1"> ' + estatus + '</td>';
      });
      html_table += '</table>';

      this.exportToExcel(html_table);

  }
  else{
    this.messasing.warning('No se encontraron polizas')
  }

}

exportToExcel(html_table:any) {
  var htmls = '';
  var uri = 'data:application/vnd.ms-excel;base64,';
  var template =
    '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>';
  var base64 = function (s:any) {
    return window.btoa(unescape(encodeURIComponent(s)));
  };

  var format = function (s:any, c:any) {
    return s.replace(/{(\w+)}/g, function (m:any, p:any) {
      return c[p];
    });
  };

  htmls = html_table;

  var ctx = {
    worksheet: 'Pólizas',
    table: htmls,
  };
  var link = document.createElement('a');
  var nombre= 'CatalogoPólizas'
  var fecha = new Date()
  link.download =
    nombre + '_' + this.obtenerFechaArchivo(fecha) + '.xls';
  link.href = uri + base64(format(template, ctx));
  link.click();
}
//LLAMADOS API
desactivarPoliza(data: any) {

  this.loadingService.present();
 this.PolizasService.desactivarPoliza(data.idPoliza )
   .then((response: any) => {
      if(response.estatus){
        this.messasing.success("Se eliminó correctamente la póliza "+data.idPoliza)
        this.obtenerPolizasPaginado();
        this.obtenerPolizas();
      }
      else{
        this.messasing.warning("No se pudo eliminar la póliza "+data.idPoliza)
      }
     this.loadingService.dismiss();

   })
   .catch((e) => {
    console.log(e);
    this.messasing.error("Ha ocurrido un error al intentar eliminar la póliza.")
     this.loadingService.dismiss();

   });
}
obtenerPolizasPaginado(){
  let data = new Poliza();
  data.busqueda=this.busqueda;
  data.paginado=this.paginado;
  data.pagina=this.pagina;
  this.loadingService.present();
 this.PolizasService.obtenerPolizasPaginado(data)
   .then((response: any) => {
      if(response.estatus){
        if(response.polizas.length){
          this.listadoPolizas=response.polizas
          this.total_paginas=this.listadoPolizas[0].total_paginas
        }
        else{
          this.messasing.warning("No se encontrarón pólizas")
        }
      }
      else{
        this.messasing.warning("No se pudo obtener el paginado")
      }
     this.loadingService.dismiss();

   })
   .catch((e) => {
    console.log(e);
    this.messasing.error("Ha ocurrido un error al consultar la póliza.")
     this.loadingService.dismiss();

   });
}
obtenerPolizas(){

  this.loadingService.present();
 this.PolizasService.obtenerPolizas()
   .then((response: any) => {
      if(response.estatus){
        if(response.polizas.length){
          this.polizasExcel=response.polizas;
        }

      }

     this.loadingService.dismiss();

   })
   .catch((e) => {
    this.messasing.error("Ha ocurrido un error al consultar la póliza")
     console.log(e);
     this.loadingService.dismiss();

   });
}

  adelantarPagina() {
    let auxPaginaActual = Number(this.pagina);

    if (auxPaginaActual + 1 <= this.total_paginas) {
      auxPaginaActual = auxPaginaActual + 1;
      this.pagina = auxPaginaActual;
      this.obtenerPolizasPaginado()
    }
  }

  adelantarUltimaPagina() {
    let auxPaginaActual = Number(this.pagina);

    if (auxPaginaActual + 1 <= this.total_paginas) {
      auxPaginaActual = this.total_paginas;
      this.pagina = auxPaginaActual;
      this.obtenerPolizasPaginado()
    }
  }

  retrocederPagina() {
    let auxPaginaActual = Number(this.pagina);

    if (auxPaginaActual - 1 >= 1) {
      auxPaginaActual = auxPaginaActual - 1;
      this.pagina = auxPaginaActual;
      this.obtenerPolizasPaginado()
    }
  }

  retrocederUltimaPagina() {
    let auxPaginaActual = Number(this.pagina);

    if (auxPaginaActual - 1 >= 1) {
      auxPaginaActual = 1;
      this.pagina = auxPaginaActual;
      this.obtenerPolizasPaginado()
    }
  }

  irPagina(pagina: number) {
    this.pagina = pagina;

    if (this.paginaAux) {
      this.obtenerPolizasPaginado()
    }
  }
  //LEVANTAR MODAL
  async crearPoliza(){
      const modal = await this.ModalController.create({
        component: CrearPolizaComponent,
        cssClass: 'my-custom-class',
        backdropDismiss: false,
        // componentProps: {

        // },
      });

      modal.onDidDismiss().then((response) => {
        this.busqueda="";
        this.pagina=1;
     this.obtenerPolizasPaginado();
     this.obtenerPolizas();

      });
      return await modal.present();
    }

    async actualizarEmpleado(element){
      const modal = await this.ModalController.create({
        component: ActualizarEmpleadoComponent,
        cssClass: 'actualizar-empleado',
        backdropDismiss: false,
        componentProps: {
          id:element.idEmpleado
        },
      });

      modal.onDidDismiss().then((response) => {
        this.busqueda="";
        this.pagina=1;
     this.obtenerPolizasPaginado();
     this.obtenerPolizas();

      });
      return await modal.present();
    }


}


