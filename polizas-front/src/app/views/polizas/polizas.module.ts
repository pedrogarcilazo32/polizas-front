import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PolizasPageRoutingModule } from './polizas-routing.module';

import { PolizasPage } from './polizas.page';
import { MatTooltipModule } from '@angular/material/tooltip';
import { CrearPolizaComponentModule } from '../components/crear-poliza/crear-poliza.component.module';
import { ActualizarEmpleadoComponentModule } from '../components/actualizar-empleado/actualizar-empleado.component.module';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PolizasPageRoutingModule,MatTooltipModule,
    CrearPolizaComponentModule,
    ActualizarEmpleadoComponentModule
  ],
  declarations: [PolizasPage]
})
export class PolizasPageModule {}
