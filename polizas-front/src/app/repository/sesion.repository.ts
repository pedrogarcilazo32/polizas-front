import { Injectable } from '@angular/core';
// import { Sesion } from '../models/sesion.model';
// import { Storage } from '@ionic/storage';

@Injectable({
    providedIn: 'root'
  })
  export class SesionRepository {
  
    // constructor(private storage: Storage) {
    //     this.storage.create();
    //  }
  constructor(){}
    // async limpiarStorage() {
    //   this.storage.clear();
    // }
 
    mandarStorage(columna: any, valor: any) {
      localStorage.setItem(columna, valor);
     
    }
  
    // eliminarStorage(columna: any) {
    //   this.storage.remove(columna);
    // }
    // limpiarSesion(){
    //   localStorage.clear();
    // }
    // obtenerSesion() {
    //   return new Promise((resolve, reject) => {
    //     this.storage.get('pnUsuarioId').then((val) => {
    //       if (val != []) {
    //         resolve(val);
    //       } else {
    //         resolve(null);
    //       }
    //     }).catch(error => {
    //       reject(error);
    //     });
    //   });
    // }
  
    obtenerValor(name: string) {
      return localStorage.getItem(name);
      // return new Promise((resolve, reject) => {
      //   this.storage.get(name).then((val) => {
      //     if (val != []) {
      //       resolve(val);
      //     } else {
      //       resolve(null);
      //     }
      //   }).catch(error => {
      //     reject(error);
      //   });
      // });
    }
  
  }
  