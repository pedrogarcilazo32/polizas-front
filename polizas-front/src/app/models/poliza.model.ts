export class Poliza{

  idPoliza?:number;
  idEmpleado?:number;
  sku_cod?:string;
  cantidad?:number;
  activo_opc?:boolean;
  fecha_reg?:string;
  nombreEmpleado?:string;
  busqueda?:string;
  total_paginas?:number;
  pagina?:number;
  paginado?:number;
}
