export class Empleado {
  idEmpleado?: number;
  nombre?: string;
  apellido?: string;
  idPuesto?: number;
  activo_opc?: boolean;
  fecha_reg?: string;
  puestoNombre?:string;
}
