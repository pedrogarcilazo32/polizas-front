export const environment = {
  production: true,
  url: 'https://competitivo.com.mx/apivg/',
  baseImgOficio: "http://192.168.1.74/SATES/api/oficios/",
  baseOficioTest: "https://competitivo.com.mx/vigilanciaplus/",
  version: "1.0.4"
};
